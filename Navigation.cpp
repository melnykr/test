//
// Created by Melnyk Roman on 8/11/18.
//

#include "Navigation.h"
#include "logger.h"

void Navigation::processKey(SDL_Keycode key, bool key_down) {
    switch (key) {
        case SDLK_LEFT: {
            rotate_speed_y = -key_down;
            Logger(LogLevel::Info) << "left key";
            break;
        }
        case SDLK_RIGHT: {
            rotate_speed_y = key_down;
            Logger(LogLevel::Info) << "right key";
            break;
        }
        case SDLK_UP: {
            Logger(LogLevel::Info) << "up key";
            rotate_speed_x = -key_down;
            break;
        }
        case SDLK_DOWN: {
            Logger(LogLevel::Info) << "down key";
            rotate_speed_x = key_down;
            break;
        }
        case SDLK_EQUALS: {
            if (key_down) {
                distance_ -= 1;
            }
            break;
        }
        case SDLK_MINUS: {
            if (key_down) {
                distance_ += 1;
            }
            break;
        }
        default:
            break;
    }
}

void Navigation::updateView() {
    rotate_x += rotate_speed_x;
    rotate_y += rotate_speed_y;
    glTranslatef(0.0f, 0.0f, -distance_);
    glRotatef(rotate_x, 1.0, 0, 0);
    glRotatef(rotate_y, 0, 1.0, 0);

}