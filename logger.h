#pragma once

#include <iostream>

enum class LogLevel {
    Debug,
    Info,
    Error
};

class Logger {
 public:
    Logger(LogLevel level) : level_(level) {}
    Logger() : Logger(LogLevel::Info) {}

    template <class T>
    Logger& operator<<(const T& data) {
        if (level_ == LogLevel::Error) {
            std::cerr << data << std::endl;
        } else if (level_ >= curr_level_) {
            std::cout << data << std::endl;;
        }
        return *this;
    }


    static LogLevel curr_level_;
    LogLevel level_;
};

