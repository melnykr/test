//
// Created by Melnyk Roman on 8/11/18.
//
#pragma once

#include <vector>

class Light {
    std::vector<bool> state;
 public:
    Light();
    void init();
    void process(char key);
};
