//
// Created by Melnyk Roman on 8/11/18.
//

#pragma once
#include "Light.h"
#include "Navigation.h"

class Engine {
 public:
    Engine();
    ~Engine();

    void ProcessEvents();
    void run();

 private:
    Light light;
    Navigation navi_;
    bool terminate_{false};

    SDL_Window* window_;
    SDL_Renderer* renderer_;
};


