//
// Created by Melnyk Roman on 8/11/18.
//

#include "Model.h"
#include "openglHelpers.h"


void MakeCube() {
    /* Cube Top */
//    GLfloat color1[] = {1.0f, 0.0f, 0.0f, 1.0f};
    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
//    glMaterialfv(GL_FRONT,GL_DIFFUSE, color1);
    glVertex3f(-1.0f, 1.0f, 1.0f);
//    glMaterialfv(GL_FRONT,GL_DIFFUSE, color1);
    glVertex3f(-1.0f, 1.0f, -1.0f);
//    glMaterialfv(GL_FRONT,GL_DIFFUSE, color1);
    glVertex3f(1.0f, 1.0f, -1.0f);
//    glMaterialfv(GL_FRONT,GL_DIFFUSE, color1);
    glVertex3f(1.0f, 1.0f, 1.0f);


    /* Cube Bottom */
    glColor4f(1.0f, 0.5f, 0.0f, 1.0f);
    glVertex3f(-2.0f, -2.0f, 2.0f);
    glVertex3f(-1.0f, -2.0f, -1.0f);
    glVertex3f(1.0f, -2.0f, -1.0f);
    glVertex3f(1.0f, -2.0f, 1.0f);

    /* Cube Front */
    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, -2.0f, 1.0f);
    glVertex3f(-2.0f, -2.0f, 2.0f);

    /* Cube Back */
    glColor4f(0.0f, 1.0f, 0.5f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, -2.0f, -1.0f);
    glVertex3f(-1.0f, -2.0f, -1.0f);

    /* Cube Left Side */
    glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-2.0f, -2.0f, 2.0f);
    glVertex3f(-1.0f, -2.0f, -1.0f);

    /* Cube Right Side */
    glColor4f(0.15f, 0.25f, 0.75f, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, -2.0f, 1.0f);
    glVertex3f(1.0f, -2.0f, -1.0f);
}