#include <iostream>
#include <unistd.h>

#include "openglHelpers.h"
#include "logger.h"
#include "Model.h"
#include "Engine.h"

int main() {
    Engine engine;
    engine.run();
    return 0;
}
