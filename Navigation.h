//
// Created by Melnyk Roman on 8/11/18.
//

#pragma once
#include "openglHelpers.h"

class Navigation {
    GLfloat rotate_speed_x = 0;
    GLfloat rotate_speed_y = 0;
    GLfloat rotate_x = 0;
    GLfloat rotate_y = 0;
    GLfloat distance_{20};
 public:
    void processKey(SDL_Keycode key, bool key_down);
    void updateView();
};