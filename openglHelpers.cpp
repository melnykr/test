//
// Created by Melnyk Roman on 8/11/18.
//

#include "openglHelpers.h"
#include "logger.h"


SDL_Texture* LoadImage(const std::string& file, SDL_Renderer* ren) {
    SDL_Surface* surface = SDL_LoadBMP("../images/1.bmp");

    if (!surface) {
        Logger(LogLevel::Error) << "Surface could not be created: " << SDL_GetError();
        return nullptr;
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(ren, surface);

    SDL_FreeSurface(surface);
    return texture;
}


void CheckVersionSdl() {
    SDL_version compiled;
    SDL_version linked;

    SDL_VERSION(&compiled);
    SDL_GetVersion(&linked);
    printf("We compiled against SDL version %d.%d.%d ...\n",
           compiled.major, compiled.minor, compiled.patch);
    printf("But we are linking against SDL version %d.%d.%d.\n",
           linked.major, linked.minor, linked.patch);
}

bool openglHelpers::init(SDL_Window** window, SDL_Renderer **ren, SDL_Texture** texture) {
    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO) < 0 ) {
        Logger(LogLevel::Error) << "Unable to init SDL: " << SDL_GetError();
        return false;
    }

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
//    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
//    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 6);
//    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
//    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1); // these brakes everything
//    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // TODO: find what does this code actually do
    *window = SDL_CreateWindow("test render",
                               100,
                               100,
                               640,
                               480,
                               SDL_WINDOW_SHOWN);
    std::cerr << "darova2" << std::endl;

    if (*window == NULL) {
        Logger(LogLevel::Error) << "Window could not be created: " << SDL_GetError();
        return false;
    }

//    *surface = SDL_GetWindowSurface(*window);
    std::cerr << "darova innit" << std::endl;

    *ren = SDL_CreateRenderer(*window, -1,  SDL_RENDERER_PRESENTVSYNC);
    std::cerr << "darova148" << std::endl;
    if (*ren == nullptr){
        Logger(LogLevel::Error) << "SDL_CreateRenderer Error: " << SDL_GetError();
        return false;
    }

//    *texture = LoadImage("../images/1.bmp", *ren);
//
//    if (*texture == nullptr) {
//        Logger(LogLevel::Error) << "Texture could not be created: " << SDL_GetError();
//        return false;
//    }
    glEnable(GL_DEPTH_TEST); //OpenGL keep track of where rendered privitives exist in the world
    glDepthFunc(GL_LEQUAL);
    return true;
}

bool openglHelpers::opengl_init()
{
    glClearColor(0, 0, 0, 0);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (640.0/480.0), 0.1, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if(glGetError() != GL_NO_ERROR)
    {
        return false;
    }

    return true;
}
