//
// Created by Melnyk Roman on 8/11/18.
//
#pragma once

#include "SDL.h"
#include "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk/System/Library/Frameworks/OpenGL.framework/Headers/gl.h"
#include "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk/System/Library/Frameworks/OpenGL.framework/Headers/glu.h"


namespace openglHelpers {
    bool init(SDL_Window** window, SDL_Renderer **ren, SDL_Texture** texture);
    bool opengl_init();
};

