//
// Created by Melnyk Roman on 8/11/18.
//

#include "Engine.h"
#include "openglHelpers.h"
#include "logger.h"
#include "Model.h"

void Engine::ProcessEvents() {
    /****** Check for Key & System input ******/
    SDL_Event event;

    while(SDL_PollEvent(&event)) {
        /******  Application Quit Event ******/
        auto key = event.key.keysym.sym;

        if(event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && key == SDLK_ESCAPE)) {
            terminate_ = true;
            return;
        }

        if (key >= '1' && key <= '3' && event.type == SDL_KEYDOWN) {
            light.process(key);
        }
        navi_.processKey(key, event.type == SDL_KEYDOWN);
    }
}

void Engine::run() {

    while(!terminate_) {

        /****** Get Most Current Time ******/
        int frame_start_time = SDL_GetTicks();

        /****** Draw Rectangle ******/
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//        SDL_RenderClear(renderer);

        navi_.updateView();

        glBegin(GL_QUADS);

        MakeCube();
        glEnd();
        glLoadIdentity();


        ProcessEvents();

        SDL_GL_SwapWindow(window_);

        /****** Frame Rate Handle ******/
        const int kNeededFps = 60;
        int delay = static_cast<int>(1000 / kNeededFps) - (SDL_GetTicks() - frame_start_time);
        if (delay > 0) {
            SDL_Delay(static_cast<uint32_t>(delay));
        }
    }
}

Engine::Engine() {
    window_ = nullptr;
    renderer_ = nullptr;
    SDL_Texture* texture = nullptr;

    if (!openglHelpers::init(&window_, &renderer_, &texture)) {
        SDL_Quit();
        std::terminate();
    }

//    SDL_RenderClear(render);
//    SDL_RenderCopy(render, texture, NULL, NULL);
//    SDL_RenderPresent(render);
    Logger(LogLevel::Info) << "SDL_init ok";
    openglHelpers::opengl_init();
    Logger(LogLevel::Info) << "opengl ok";
    //    SDL_DestroyTexture(texture);
    light.init();
}

Engine::~Engine() {
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(window_);
    SDL_Quit();
}