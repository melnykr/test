//
// Created by Melnyk Roman on 8/11/18.
//

#include "Light.h"
#include "openglHelpers.h"
#include "logger.h"

Light::Light()  {
    state.resize(3);
    Logger(LogLevel::Debug) << "Start init lighting";

}

void Light::init() {
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    GLfloat global_ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);
    glShadeModel(GL_SMOOTH);
    GLfloat specular[] = {1.0f, 1.0f, 1.0f , 1.0f};
    glLightfv(GL_LIGHT1, GL_AMBIENT, specular);
    GLfloat position[] = { -100.f, 0.f, 0.f, 1.0f };
    glLightfv(GL_LIGHT1, GL_POSITION, position);
    Logger(LogLevel::Debug) << "Done init lighting";
}

void Light::process(char key) {
    key -= '0';
    auto light = GL_LIGHT0 + key;
    state[key] = !state[key];

    if (state[key]) {
        Logger(LogLevel::Info) << "Enable light " << int(key);
        glEnable(light);
    } else {
        Logger(LogLevel::Info) << "Disable light " << int(key);
        glDisable(light);
    }
}